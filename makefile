LDLIBS += -lpcap

all: pcap-test

pcap-test: utils.c pcap-test.c

clean:
	rm -f pcap-test *.o
